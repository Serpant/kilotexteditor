#ifndef KILO_EDITOR_KEY_H
#define KILO_EDITOR_KEY_H

// plain enum because enum is easily converting to int
// instead of using class enum and cast using int(Key)
// better readability
struct Key {
    enum KeyName {
        enter = 13,
        esc = 27,
        backspace = 127,
        arrow_left = 1000,  // prevent collision with other ASCII keys
        arrow_right,
        arrow_up,
        arrow_down,

        del,
        home,
        end,

        page_up,
        page_down
    };
};

#endif  // KILO_EDITOR_KEY_H
