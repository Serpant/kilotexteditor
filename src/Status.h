#ifndef STATUS_H
#define STATUS_H

#include <chrono>
#include <string>

class Status {
public:
    explicit Status(std::string start_msg)
        : message{std::move(start_msg)},
          timestamp{std::chrono::system_clock::now()} {}
    [[nodiscard]] auto get_message() const -> const std::string& {
        return message;
    }

    [[nodiscard]] auto get_timestamp() const
        -> std::chrono::time_point<std::chrono::system_clock> {
        return timestamp;
    }

    double elapsed_time() const {
        std::chrono::duration<double> elapsed_time =
            std::chrono::system_clock::now() - timestamp;
        return elapsed_time.count();
    }

    void set_message(std::string_view new_msg) {
        message = new_msg;
        timestamp = std::chrono::system_clock::now();
    };

private:
    std::string message;
    std::chrono::time_point<std::chrono::system_clock> timestamp;
};

#endif  // STATUS_H
