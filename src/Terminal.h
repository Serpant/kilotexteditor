#ifndef TERMINAL_H
#define TERMINAL_H

#include <termio.h>
#include <unistd.h>

#include <list>
#include <sstream>
#include <string_view>
#include <vector>

#include "Document.h"
#include "Mark.h"
#include "Screen.h"

using Line = std::vector<Extended_char>;

class Terminal {
public:
    ~Terminal();

    Terminal(const Terminal&) = delete;
    Terminal& operator=(const Terminal&) = delete;
    Terminal(Terminal&&) = delete;
    void operator=(Terminal&&) = delete;

    static Terminal& get();

    void attach(Document& doc) {
        get().mark.attach(doc);
        get().screen.attach(mark, doc);
    }

    std::string prompt(const std::string& prompt);

    inline void set_message(std::string_view msg) {
        screen.status.set_message(msg);
        screen.refresh();
    }

    Screen screen;
    Mark mark;

private:
    Terminal();

    void enableRawMode();
    void disableRawMode();

    termios old_term_settings;
};

#endif  // TERMINAL_H
