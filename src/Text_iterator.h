#ifndef TEXT_ITERATOR_H
#define TEXT_ITERATOR_H

#include <list>
#include <vector>

#include "Extended_char.h"

class Document;

using Line = std::vector<Extended_char>;

class Text_iterator {
public:
    // TODO: get rid of third argument
    Text_iterator(std::list<Line>::iterator line, Line::size_type index,
                  std::list<Line>::iterator end)
        : ln_{line}, index_{index}, end_{end} {}

    char& operator*() { return *(ln_->begin() + index_); }
    Text_iterator& operator++();

    bool operator==(const Text_iterator& other) const {
        return ln_ == other.ln_ && index_ == other.index_;
    }
    bool operator!=(const Text_iterator& other) const {
        return !(*this == other);
    }

    bool isInEmptyLine() const { return ln_->empty(); }

    auto getY() const { return ln_; }
    auto getX() const { return index_; }
    auto getXIter() const { return ln_->begin() + index_; }

private:
    // keep track of line and character position within a line
    std::list<Line>::iterator ln_;
    Line::size_type index_;

    std::list<Line>::iterator end_;
};

#endif  // TEXT_ITERATOR_H
