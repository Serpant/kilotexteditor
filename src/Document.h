#ifndef DOCUMENT_H
#define DOCUMENT_H

#include <algorithm>
#include <filesystem>
#include <fstream>
#include <list>
#include <string>
#include <vector>

#include "Extended_char.h"
#include "Text_iterator.h"

using Line = std::vector<Extended_char>;

enum class FileType {
    None,
    c,
    cpp,
    python,
    go,
    rust,
};

class Document {
public:
    Document();
    explicit Document(std::filesystem::path file);

    [[nodiscard]] inline bool empty() const {
        // if multiple lines then is not empty
        // or if first line contains chracters
        if (lines.size() > 1 or not lines.front().empty()) {
            return false;
        }
        if (not lines.front().empty()) {
            return false;
        }

        return true;
    }

    inline auto lineBegin() { return lines.begin(); }
    inline auto lineEnd() { return lines.end(); }

    // returns first character
    // if empty return end
    Text_iterator begin() {
        Text_iterator first{lines.begin(), 0, lines.end()};
        while (first != end() && first.isInEmptyLine()) {
            ++first;  // skip empty lines
        }

        return first;
    }

    Text_iterator end() { return Text_iterator{lines.end(), 0, lines.end()}; }

    [[nodiscard]] inline auto size() const { return lines.size(); }

    inline void setDirty() { dirty_ = true; }
    [[nodiscard]] inline bool isDirty() const { return dirty_; }

    [[nodiscard]] std::string getExtension() const;
    [[nodiscard]] FileType getFileType() const;

    auto save(const std::string& new_filename = "") -> uintmax_t;

    inline void setName(const std::string& new_name) {
        std::filesystem::rename(file_, new_name);
    }
    [[nodiscard]] inline std::string name() const { return file_.filename(); }

    std::list<Line> lines;  // plain data
private:
    void tab_to_spaces();

    std::filesystem::path file_;
    bool dirty_;
};

class Abort_save : public std::exception {
public:
    explicit Abort_save(const char* msg) : message{msg} {}
    explicit Abort_save(std::string msg) : message{std::move(msg)} {}

    [[nodiscard]] const char* what() const noexcept override {
        return message.c_str();
    }

private:
    std::string message;
};

#endif  // DOCUMENT_H
