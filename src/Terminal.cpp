#include "Terminal.h"

#include "Key.h"
#include "kilo_tools.h"
#include "read_key.h"

using std::string;

void Terminal::enableRawMode() {
    tcgetattr(STDIN_FILENO, &old_term_settings);

    // set flags
    termios raw = old_term_settings;
    raw.c_iflag &= ~(ISTRIP | BRKINT | INPCK | ICRNL | IXON);
    raw.c_iflag &= ~(OPOST);
    raw.c_cflag &= (CS8);
    raw.c_lflag &= ~(ECHO | ICANON | IEXTEN | ISIG);

    tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw);
}

// read out old saved terminal settings
void Terminal::disableRawMode() {
    tcsetattr(STDIN_FILENO, TCSAFLUSH, &old_term_settings);
}

Terminal::Terminal() {
    enableRawMode();

    cursor_move(0, 0);
}

Terminal::~Terminal() { disableRawMode(); }

Terminal& Terminal::get() {
    static Terminal term;

    return term;
}

std::string Terminal::prompt(const std::string& prompt) {
    string response;
    response.reserve(screen.columns() / 2);

    while (true) {
        set_message(prompt + ": '" + response + "'");
        screen.refresh();

        int ch = readKey();
        switch (ch) {
            case Key::del:
            case ctrl_key('h'):
            case Key::backspace:
                if (!response.empty()) {
                    response.pop_back();
                }
                break;
            case Key::esc:
                set_message("");
                return "";
                break;
            case Key::enter:
                if (!response.empty()) {
                    set_message("");
                    return response;
                }
                break;
            default:
                if (response.size() <
                    screen.columns() / 2) {  // prevent long response // OUGH
                    if (!iscntrl(ch) && ch < 128) response += ch;
                }
        }
    }
}
