#ifndef KILO_EDITOR_TOOLS_H
#define KILO_EDITOR_TOOLS_H

template <typename First, typename... T>
inline bool is_in(const First& first, const T&... t) {
    return ((first == t) or ...);
}

constexpr int ctrl_key(int ch) { return ch & 0x1F; }

void alternate_screen(bool change);
void cursor_visibility(bool visible);
void cursor_move(int cy, int cx);
void clear_line();

#endif  // KILO_EDITOR_TOOLS_H
