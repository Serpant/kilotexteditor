#ifndef EXTENDED_CHAR_H
#define EXTENDED_CHAR_H

#include <iostream>

enum class HighlightType {
    normal = 0,
    number,
    match,
    hl_string,
    comment,
    keyWord,
    typeKeyWord,
    preprocessor,
};

std::ostream& operator<<(std::ostream& os, HighlightType type);

class Extended_char {
public:
    Extended_char(char c) : ch{c} {}
    char ch;

    /* implicit */ operator char&() { return ch; }  // be reference
    /* implicit */ operator const char&() const {
        return ch;
    }  // by const-refernce
       // /* implicit */ operator char() { return ch; }  // by copy

    /* implicit */ operator int() { return ch; }  // to int

    bool operator==(char other) const { return ch == other; }
    bool operator!=(char other) const { return ch != other; }

    bool operator==(int other) const { return ch == other; }
    bool operator!=(int other) const { return ch != other; }

    friend std::ostream& operator<<(std::ostream& os, const Extended_char& ch);

    auto getHighLight() const { return hltype; }
    void setHighLight(HighlightType t) { hltype = t; };

private:
    HighlightType hltype = HighlightType::normal;  // default
};

#endif  // EXTENDED_CHAR_H
