#include "Extended_char.h"

#include "Text_modifiers.h"

std::ostream& operator<<(std::ostream& os, const Extended_char& ext_char) {
    if (ext_char.hltype == HighlightType::normal)
        os << ext_char.ch;
    else
        os << ext_char.hltype << ext_char.ch << TextColor::reset;

    return os;
}

std::ostream& operator<<(std::ostream& os, HighlightType type) {
    switch (type) {
        case HighlightType::number:
            os << TextColor::blue;
            break;
        case HighlightType::match:
            os << TextColor::reverse;
            break;
        case HighlightType::hl_string:
            os << TextColor::green;
            break;
        case HighlightType::comment:
            os << TextColor::cyan;
            break;
        case HighlightType::typeKeyWord:
            os << TextColor::yellow;
            break;
        case HighlightType::keyWord:
            os << TextColor::purple;
            break;
        case HighlightType::preprocessor:
            os << TextColor::blue;
        case HighlightType::normal:
            // do nothing
            break;
    }

    return os;
}
