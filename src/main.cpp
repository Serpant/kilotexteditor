/*  KITO TEXT EDITOR links which I used
--> Document structure Chapter 20.6 Simple text editor
 * https://www.stroustrup.com/programming.html
--> Kilo text editor tutorial
 * https://viewsourcecode.org/snaptoken/kilo/04.aTextViewer.html
--> ANSI escape codes
 * https://gist.github.com/fnky/458719343aabd01cfb17a3a4f7296797
 * https://notes.burke.libbey.me/ansi-escape-codes/
*/

#include <iostream>
#include <vector>

#include "Document.h"
#include "Extended_char.h"
#include "Terminal.h"
#include "editor.h"

using Line = std::vector<Extended_char>;

int main(int argc, char* argv[]) try {
    Terminal::get();  // prepare environment

    std::vector<std::string> filenames(argv + 1, argv + argc);
    Editor::TextEditor kiloEditor{filenames};
    Terminal::get().attach(kiloEditor.file());

    kiloEditor.run();
} catch (const std::runtime_error& e) {
    std::cerr << e.what() << '\n';
} catch (const std::exception& e) {
    std::cerr << e.what() << '\n';
    return -1;
}
