#include "Mark.h"

#include "Key.h"

void Mark::move(const int key) {
    switch (key) {
        case Key::arrow_up:
            if (m_line != m_doc->lineBegin()) {
                --m_line;
            }
            break;
        case Key::arrow_down:
            if (m_line != m_doc->lineEnd()) {
                ++m_line;
            }
            break;
        case Key::arrow_left:
            if (x != 0)
                --x;
            else if (m_line != m_doc->lineBegin()) {
                --m_line;
                x = m_line->size();
            }
            break;
        case Key::arrow_right:
            if (m_line != m_doc->lineEnd() &&  // we are not in empty line
                x < m_line->size()) {          // we have space to move
                ++x;
            } else if (x == m_line->size()) {
                ++m_line;
                x = 0;
            }
            break;
    }
}
