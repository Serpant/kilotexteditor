#include "editor.h"

#include "Key.h"
#include "kilo_settings.h"
#include "kilo_tools.h"
#include "read_key.h"

using std::list;
using std::string;

Editor::TextEditor::TextEditor(const std::vector<std::string>& filenames) {
    if (filenames.empty()) {
        files_.emplace_back();
    } else {
        for (const auto& filename : filenames) {
            files_.emplace_back(filename);
        }
    }
    file_ = files_.begin();
}
void Editor::TextEditor::prev_file() {
    if (files_.size() == 1) {
        return;
    }

    --file_;
    if (file_ == std::prev(files_.begin())) {
        file_ = std::prev(--files_.end());
    }
    Terminal::get().attach(*file_);
}
void Editor::TextEditor::next_file() {
    if (files_.size() == 1) {
        return;
    }

    ++file_;
    if (file_ == files_.end()) {
        file_ = files_.begin();
    }
    Terminal::get().attach(*file_);
}

void Editor::TextEditor::insertChar(Mark& mark, int ch) {
    if (mark.line() == file_->lineEnd()) {  // last empty line
        Editor::append_line(*file_);
        // reset mark pos to new added line
        mark.m_line = prev(file_->lineEnd());
    }

    Editor::rowInsertChar(mark.line(), mark.x, ch);

    if (ch == '\t') {
        mark.x += settings::tab_size;
    } else {
        ++mark.x;
    }

    file_->setDirty();
}

void moveMarkToBegin() { Terminal::get().mark.x = 0; }
void moveMarkToEnd() {
    if (not Terminal::get().mark.isDocumentEnd()) {
        Terminal::get().mark.x = Terminal::get().mark.line()->size();
    }
}

void Editor::TextEditor::quit() {
    if (state == State::run) {
        if (file_->isDirty()) {
            string new_msg =
                "[WARNING] file has unsaved changes. PRESS CTRL-Q "
                "to confirm quit";
            Terminal::get().set_message(new_msg);

            state = State::quit_confirmed;
        } else {
            state = State::quit;
        }
    } else if (state == State::quit_confirmed) {
        state = State::quit;
    }
}

void Editor::TextEditor::processKeyPress() {
    int ch = readKey();
    switch (ch) {
        case ctrl_key('q'):
            quit();
            return;
        case ctrl_key('s'):
            save();
            break;
        case ctrl_key('f'):
            search();
            break;
        case '\r':
            insertNewLine(Terminal::get().mark);
            break;
        case Key::home:
            moveMarkToBegin();
            break;
        case Key::end:
            moveMarkToEnd();
            break;
        case Key::backspace:
        case ctrl_key('h'):
        case Key::del:
            if (ch == Key::del) {
                Terminal::get().mark.move(Key::arrow_right);
            }
            delChar(Terminal::get().mark);
            break;
        case Key::page_up:
            markMoveScreenTop();
            break;
        case Key::page_down:
            markMoveScreenBottom();
            break;
        case ctrl_key('l'):
        case Key::esc:
            // ignore
            break;
        case ctrl_key('p'):
            prev_file();
            break;
        case ctrl_key('n'):
            next_file();
            break;
        case ctrl_key('o'):
            open_file();
            break;
        case ctrl_key('c'):
            close_file();
            if (state != State::run) {
                return;
            }
            break;
        case Key::arrow_up:
        case Key::arrow_down:
        case Key::arrow_left:
        case Key::arrow_right:
            Terminal::get().mark.move(ch);
            adjustScreenOffset();
            break;
        default:
            insertChar(Terminal::get().mark, ch);
            break;
    }

    state = State::run;  // default
}

void Editor::TextEditor::delChar(Mark& mark) const {
    if (mark.line() == file_->lineEnd()) {
        return;
    }
    if (mark.x == 0 && mark.line() == file_->lineBegin()) {
        return;
    }

    if (mark.x > 0) {
        Editor::rowDelChar(mark.line(), mark.x - 1);
        --mark.x;
        file_->setDirty();
    } else {  // x == 0
        // backspace from beggining of the line so we have to concat
        // curr_line to previous_line
        auto line = mark.line();
        auto base_line = prev(line);

        // change mark position
        mark.m_line = base_line;
        mark.x = base_line->size();

        // copy back line
        Editor::join_line(base_line, line);
        // and erase previous line
        file_->lines.erase(line);

        file_->setDirty();
    }
}

void Editor::TextEditor::insertNewLine(Mark& mark) {
    if (mark.x > mark.line()->size()) {
        return;
    }

    if (mark.line() == file_->lineEnd()) {  // we are past last line
        Editor::append_line(*file_);
        --mark.m_line;  // stay in the current line
        return;
    }

    auto base_line = mark.line();
    // insert under base_line
    auto created_line = file_->lines.insert(next(base_line), Line{});
    // insert from previous line to new line
    created_line->insert(created_line->begin(), mark.xAsIter(),
                         base_line->end());

    // after copying erase elements from previous line
    base_line->erase(mark.xAsIter(), base_line->end());

    // move mark to begining of new line
    ++mark.m_line;
    mark.x = 0;
}

void Editor::TextEditor::save() try {
    std::uintmax_t bytes = 0;
    if (file_->name().empty()) {
        std::string filename = Terminal::get().prompt("Enter filename: ");
        bytes = file_->save(filename);
    } else {
        bytes = file_->save();
    }

    Terminal::get().set_message(std::to_string(bytes) + " bytes written");
} catch (const std::exception& err) {
    Terminal::get().set_message(err.what());
}
void Editor::TextEditor::open_file() {
    auto filename = Terminal::get().prompt("Enter filename: ");
    files_.emplace_back(filename);

    file_ = std::prev(files_.end());  // focus on this file
    Terminal::get().attach(*file_);
}
void Editor::TextEditor::close_file() {
    if (file_->isDirty()) {
        Terminal::get().set_message(
            "Unsaved changes cannot close current file");
        return;
    }

    if (files_.size() > 1) {
        auto file_on_left = std::prev(files_.erase(file_));
        if (file_on_left == std::prev(files_.begin())) {
            file_on_left = std::prev(files_.end());
        }

        file_ = file_on_left;

        Terminal::get().attach(*file_);
    } else {
        quit();
    }
}

string create_search_message(
    const std::vector<Text_iterator>& positions,
    const std::vector<Text_iterator>::const_iterator pos) {
    constexpr size_t count_from_1 = 1;
    const size_t index = std::distance(positions.cbegin(), pos) + count_from_1;

    std::ostringstream msg;
    msg << "[" << index << "/" << positions.size() << "]"
        << " | HELP: CTRL-N = next | CTRL-P = previous | ESC = reset "
           "position";

    return msg.str();
}

void travel_through_positions(const std::vector<Text_iterator>& positions,
                              Mark& mark) {
    Mark old = mark;  // save position
    auto pos = positions.begin();
    while (true) {
        mark.x = pos->getX();
        mark.m_line = pos->getY();

        Terminal::get().set_message(create_search_message(positions, pos));

        switch (readKey()) {
            case ctrl_key('n'):
                ++pos;
                if (pos == positions.end()) {
                    pos = positions.begin();
                }
                break;
            case ctrl_key('p'):
                --pos;
                if (pos == prev(positions.begin())) {
                    pos = prev(positions.end());
                }
                break;
                // BUG: when escape is pressed we should immediately
                // return to previous position - currently we have to
                // press key to move back fix: readKey() functions
                // cannot know if user pressed esc or special character
                // so we must wait for next key pressed, to check if it
                // was only enter followed by plain displyable character
            case Key::esc:  // recover postion
                mark = old;
                return;
            default:  // just start typing
                std::cin.unget();
                return;
        }
    }
}

void mark_positions(const std::vector<Text_iterator>& positions,
                    const std::string::size_type length) {
    for (const auto& pos : positions) {
        auto first = pos.getXIter();  // begin of searched query
        auto end = first + length;

        // mark each character
        for_each(first, end,
                 [](auto& p) { p.setHighLight(HighlightType::match); });
    }
}

void unmark_positions(const std::vector<Text_iterator>& positions,
                      const std::string::size_type length) {
    for (const auto& pos : positions) {
        auto first = pos.getXIter();  // begin of searched query
        auto end = first + length;

        // unmark each character
        for_each(first, end,
                 [](auto& p) { p.setHighLight(HighlightType::normal); });
    }
}

void Editor::TextEditor::search() {
    std::string query = Terminal::get().prompt("Search: ");
    if (query.empty()) {
        Terminal::get().set_message("Searching aborted");
    }

    const auto positions = find_txt(file_->begin(), file_->end(), query);
    if (!positions.empty()) {
        mark_positions(positions, query.size());
        travel_through_positions(positions, Terminal::get().mark);
    } else {
        Terminal::get().set_message("Not found");
    }

    unmark_positions(positions, query.size());
}

void Editor::TextEditor::run() {
    while (is_running()) {
        Terminal::get().screen.refresh();
        processKeyPress();
    }
}

void Editor::rowDelChar(list<Line>::iterator row, Line::size_type pos) {
    if (pos > row->size()) {
        return;
    }

    row->erase(std::next(row->begin(), pos));
}

void Editor::join_line(list<Line>::iterator base, list<Line>::iterator line) {
    base->insert(base->end(), line->begin(), line->end());
}

void Editor::rowInsertChar(list<Line>::iterator row, Line::size_type at,
                           const int ch) {
    if (at > row->size()) {
        at = row->size();
    }

    auto pos = std::next(row->begin(), at);
    if (ch == '\t') {  // if tab change it to spaces
        row->insert(pos, settings::tab_size, ' ');
    } else {
        row->insert(pos, ch);
    }
}

void Editor::append_line(Document& doc) { doc.lines.emplace_back(); }

bool Editor::match(Text_iterator first, Text_iterator last, const string& query)
// match text in the line
{
    for (const char ch : query) {
        if (first == last || ch != *first) {
            return false;  // end of line first
        }

        ++first;
    }

    return true;
}

auto Editor::find_txt(Text_iterator first, Text_iterator last,
                      const string& query) -> std::vector<Text_iterator> {
    std::vector<Text_iterator> positions;
    if (query.empty()) {
        return positions;  // cannot find empty string
    }

    const char first_char = query[0];
    while (first != last) {
        if (first_char == *first && match(first, last, query)) {
            positions.push_back(first);
            // (serpant) TODO: advance first by length of string if found
        }

        ++first;
    }

    return positions;
}
