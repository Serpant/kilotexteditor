#include "Keyword.h"
#include "Terminal.h"
#include "Text_modifiers.h"
#include "kilo_settings.h"
#include "kilo_tools.h"

using std::cout;
using std::list;
using std::string;
using std::to_string;

void Screen::drawStatusMessage() const {
    constexpr int message_duration = 5;
    clear_line();
    if (status.elapsed_time() < message_duration) {
        if (status.get_message().size() > m_columns) {
            cout << status.get_message().substr(0, m_columns);
        } else {
            cout << status.get_message();
        }
    }
}

Screen::Screen()
    : status{"HELP: CTRL-S = save | CTRL-Q = exit | CTRL-F = find"},
      m_columns{getWindowSize().first},
      m_rows{getWindowSize().second - settings::status_bar_size}
// because we make space for status bar
{
    alternate_screen(true);
}

Screen::~Screen() { alternate_screen(false); }

auto Screen::linesEnd() const
// returns last line printed on the screen
{
    auto lineIterEnd = linesBegin();
    auto linesToMove = m_rows;

    while (linesToMove != 0 and lineIterEnd != m_doc->lineEnd()) {
        ++lineIterEnd;  // move to next line
        --linesToMove;
    }

    return lineIterEnd;
}

bool is_separator(int c) {
    if (isspace(c) != 0) {
        return true;
    }

    constexpr std::string_view special_chars{",.()+-/*=~%<>[];:"};
    return special_chars.find(c) != std::string_view::npos;
}

// we need full line to get context of number
// checks if char is number e.g int or double
// or if is part or [], ()
// char is not number if it is part of name e.g int64_t
bool isPartOfNumber(Line::iterator pos) {
    auto prev = std::prev(pos);  // we known that we are not in first pos
    bool prev_char_sep = is_separator(*prev);
    if (isdigit(*pos) != 0 and
        (prev_char_sep or prev->getHighLight() == HighlightType::number)) {
        return true;
    }

    // part of floating point number
    if (*pos == '.' and prev->getHighLight() == HighlightType::number) {
        return true;
    }

    return false;
}

// need surrounding chars to get context of current char
HighlightType characterType(Line::iterator first_char, Line::iterator end_char,
                            Line::iterator pos) {
    if (*pos == '/') {
        auto next = std::next(pos);
        if (next != end_char && *next == '/') {
            return HighlightType::comment;
        }
    }

    if ((pos == first_char and std::isdigit(*pos) != 0) or
        isPartOfNumber(pos)) {
        return HighlightType::number;
    }

    return HighlightType::normal;
}

// checks if words is not part of someother word e.g void -> avoid
bool isSingleWord(const std::string& line, string::size_type index,
                  std::string_view keyword) {
    auto next_char = line.begin() + index + keyword.size();
    auto prev_char = line.begin() + index - 1;

    // check if previous char is not separator
    if (index > 0 and not is_separator(*prev_char)) {
        return false;
    }
    // check if next char is not separator
    if (next_char != line.end() and not is_separator(*next_char)) {
        return false;
    }

    return true;
}

// find keywords in row and return positions of each one
auto find_keywords(Line::iterator first_char, Line::iterator end_char,
                   const FileType filetype) -> std::vector<Keyword> {
    auto keyword_set = getKeyWords(filetype);

    std::vector<Keyword> positions;
    string line{first_char, end_char};
    for (const auto& [keyword, type] : keyword_set) {
        auto index = line.find(keyword);
        while (index != std::string::npos) {
            if (isSingleWord(line, index, keyword)) {
                auto pos = first_char + index;
                if (pos->getHighLight() == HighlightType::normal) {
                    Keyword current{pos, pos + keyword.size(), type};
                    positions.push_back(current);
                }
            }
            index = line.find(
                keyword, index + keyword.size());  // start from next position
        }
    }

    return positions;
}

bool isSingleLineComment(Line::iterator first, Line::iterator last) {
    // we know that we are not in last character, so (curr_char + 1) != end()
    auto second = std::next(first);
    if (first == last or second == last) {
        return false;
    }

    if (*first == '/' and *second == '/') {
        return true;
    }

    return false;
}

void markRange(Line::iterator first, Line::iterator last, HighlightType type) {
    while (first != last) {
        first->setHighLight(type);
        ++first;
    }
}

void Screen::rowsUpdateSyntax() {
    FileType type = m_doc->getFileType();
    if (type == FileType::None) {
        return;
    }

    for (auto row = linesBegin(); row != linesEnd(); ++row) {
        for (auto& ch : *row) {
            ch.setHighLight(HighlightType::normal);
        }
    }

    // int prev_sep = 1;
    int in_string = 0;
    for (auto row = linesBegin(); row != linesEnd(); ++row) {
        for (auto curr_char = row->begin(); curr_char != row->end();) {
            char c = *curr_char;
            /*
            auto prev_hl = (curr_char != row->begin())
                               ? (curr_char - 1)->getHighLight()
                               : HighlightType::normal;
                */
            if (in_string) {
                auto end_quote = find(curr_char, row->end(), in_string);
                if (end_quote != row->end()) {  // ending char
                    markRange(curr_char, end_quote, HighlightType::hl_string);

                    in_string = 0;
                    curr_char = end_quote;
                    // continue;
                } else {  // no ending char - string end probably in next line
                    markRange(curr_char, row->end(), HighlightType::hl_string);
                    break;  // move to next line
                }
            } else {
                if (c == '"' || c == '\'') {
                    in_string = c;
                    curr_char->setHighLight(HighlightType::hl_string);
                    ++curr_char;
                    continue;
                }
            }

            if (isSingleLineComment(curr_char, row->end())) {
                markRange(curr_char, row->end(), HighlightType::comment);
                break;  // we filled up whole line
            }

            if (isPartOfNumber(curr_char)) {
                curr_char->setHighLight(HighlightType::number);
                ++curr_char;
                // prev_sep = 0;
                continue;
            }

            // prev_sep = is_separator(c);
            ++curr_char;
        }

        auto keywords_pos = find_keywords(row->begin(), row->end(), type);
        for (auto& kword : keywords_pos) {
            markRange(kword.begin(), kword.end(), kword.getHighLight());
        }
    }
}

void Screen::refresh() {
    scroll();

    cursor_visibility(false);  // hide to avoid flickering on Screen
    cursor_move(0, 0);

    rowsUpdateSyntax();
    drawRows();
    drawStatusBar();
    drawStatusMessage();

    // recover position within window
    cursor_move(m_mark->yAsInt() - lineNumber(), m_mark->x - column_offset);
    cursor_visibility(true);
}

void prepareNextLine() {
    clear_line();
    cout << "\r\n";  // move to next line
}

void Screen::drawStatusBar() {
    cout << TextColor::reverse;

    string lstatus;
    string rstatus;
    // format name
    if (m_doc->name().empty()) {
        lstatus = settings::no_name;
    } else {
        if (m_doc->name().size() > settings::name_max_size) {
            lstatus =
                m_doc->name().substr(0, settings::name_max_size - 3) + "...";
        } else {
            lstatus = m_doc->name();
        }
    }
    lstatus += m_doc->isDirty() ? " (modified)" : "";
    lstatus += " - " + to_string(m_doc->size()) + " lines";

    FileType type = m_doc->getFileType();
    if (type == FileType::None) {
        rstatus += "no filetype ";
    } else {
        rstatus += m_doc->getExtension() + " file";
    }
    rstatus += " | " + to_string(m_mark->yAsInt() + 1) + ":" +
               to_string(m_mark->x + 1);
    int inner_fill_size = m_columns - lstatus.size() - rstatus.size();

    // print status bar
    cout << lstatus << string(inner_fill_size, ' ') << rstatus;
    cout << TextColor::reset;

    prepareNextLine();
}

// draws empty line
void draw_empty_lines(std::list<Line>::size_type lines_left) {
    constexpr char empty_line_sign = '~';

    for (std::list<Line>::size_type i = 0; i < lines_left; ++i) {
        cout << empty_line_sign;
        prepareNextLine();
    }
}
// (Serpant) TODO: welcome message
// using printWelcomeMsg();
// (Serpant) TODO: split inner function to smaller ones
void Screen::drawRows() const {
    auto empty_lines = m_rows;
    for (auto curr_line = linesBegin(); curr_line != linesEnd(); ++curr_line) {
        auto first_char = curr_line->begin() + column_offset;
        auto screen_border = first_char + m_columns;
        auto end_char =
            screen_border > curr_line->end() ? curr_line->end() : screen_border;

        print_line(first_char, end_char);

        --empty_lines;
        prepareNextLine();
    }
    draw_empty_lines(empty_lines);
}

auto distance_to_next_word(std::list<Line>::iterator line,
                           Line::iterator current) {
    size_t distance = 0;
    while (current != line->end() and *current != ' ') {
        ++distance;
        ++current;
    }

    return distance;
}

void Screen::scroll()
// we change firstPrintedLine relative to mark pos instead of decrementing
// because mark can go free when searching
{
    auto line_number = lineNumber();

    if (m_mark->yAsInt() < line_number)
        firstPrintedLine = m_mark->line();  // --firstPrintedLine
    else if (m_mark->yAsInt() >= line_number + m_rows) {
        firstPrintedLine =
            prev(m_mark->line(), m_rows - 1);  // ++firstPrintedLine
    }

    if (m_mark->x < column_offset) {
        column_offset = m_mark->x;
    } else if (m_mark->x >= column_offset + m_columns) {
        column_offset = m_mark->x - m_columns + 1;
        column_offset +=
            distance_to_next_word(m_mark->line(), m_mark->xAsIter());
    }
}

auto getWindowSize() -> std::pair<Line::size_type, std::list<Line>::size_type> {
    winsize ws;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &ws);

    return std::make_pair(ws.ws_col, ws.ws_row);
}

void adjustScreenOffset() {
    Screen& screen = Terminal::get().screen;
    Mark& mark = Terminal::get().mark;

    if (mark.line() == screen.linesEnd()) {
        mark.x = 0;
    } else if (mark.x > mark.line()->size()) {
        screen.setColumnOffest(0);
        mark.x = mark.line()->size();  // set mark to end of line
    }
}

void printWelcomeMsg(int columns, std::string_view welcome) {
    int padding = (columns - welcome.size()) / 2;
    if (padding != 0) {
        cout << "~";
        --padding;  // minus '~' character
    }
    cout << string(--padding, ' ') << welcome;
}

auto getLongestScreenLineSize(const Screen& screen) {
    auto longest = screen.linesBegin()->size();
    for (auto line = screen.linesBegin(); line != screen.linesEnd(); ++line) {
        if (longest < line->size()) {
            longest = line->size();
        }
    }

    return longest;
}

void markMoveScreenTop() {
    Screen& screen = Terminal::get().screen;
    Mark& mark = Terminal::get().mark;

    mark.m_line = screen.linesBegin();
    if (mark.x > mark.line()->size()) {
        mark.x = mark.line()->size();
    }
}

void markMoveScreenBottom() {
    Screen& screen = Terminal::get().screen;
    Mark& mark = Terminal::get().mark;

    mark.m_line = screen.linesEnd();

    if (mark.isDocumentEnd()) {  // prevent going outside of file
        mark.x = 0;
    } else if (mark.x > mark.line()->size()) {
        mark.x = mark.line()->size();
    }
}

void print_control_character(char c) {
    constexpr int max_ctrl = 26;
    char sym = (c <= max_ctrl) ? '@' + c : '?';  // is char known
    cout << BgColor::Red << sym << TextColor::reset;
}

void print_line(Line::iterator first, Line::iterator last) {
    while (first < last) {
        if (std::iscntrl(*first) != 0) {
            print_control_character(*first);
        } else {
            cout << *first;
        }
        ++first;
    }

    clear_line();  // clears junk characters that were drawn before
}
