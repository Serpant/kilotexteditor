#ifndef MARK_H
#define MARK_H

#include <list>
#include <memory>
#include <vector>

#include "Document.h"
#include "Extended_char.h"

using Line = std::vector<Extended_char>;

class Mark {
public:
    Mark(){};

    inline std::list<Line>::size_type yAsInt() const {
        return std::distance(m_doc->lineBegin(), m_line);
    }
    inline Line::iterator xAsIter() const { return m_line->begin() + x; }

    inline std::list<Line>::iterator line() const { return m_line; }
    void move(const int key);

    void attach(Document& doc) {
        m_doc = &doc;

        m_line = m_doc->lineBegin();
        x = 0;
    }

    bool isDocumentEnd() const { return m_line == m_doc->lineEnd(); }

    Line::size_type x;
    std::list<Line>::iterator m_line;

private:
    Document* m_doc;
};

#endif  // MARK_H
