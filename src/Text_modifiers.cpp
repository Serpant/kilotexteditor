#include "Text_modifiers.h"

std::ostream& operator<<(std::ostream& os, TextColor c) {
    os << "\033[" << static_cast<int>(c) << "m";

    return os;
}

std::ostream& operator<<(std::ostream& os, BgColor c) {
    os << "\033[" << static_cast<int>(c) << "m";

    return os;
}

std::ostream& operator<<(std::ostream& os, TextStyle s) {
    os << "\033[" << static_cast<int>(s) << "m";

    return os;
}
