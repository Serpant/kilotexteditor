#ifndef KILO_EDITOR_SETTINGS_H
#define KILO_EDITOR_SETTINGS_H

#include <string_view>

namespace settings {
constexpr int tab_size = 4;
constexpr int status_bar_size = 3;
constexpr int name_max_size = 20;
constexpr std::string_view no_name = "[No Name]";
}  // namespace settings

#endif  // KILO_EDITOR_SETTINGS_H
