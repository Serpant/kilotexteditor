#include "Document.h"

#include <filesystem>

#include "kilo_settings.h"
#include "kilo_tools.h"

using std::string;
using std::vector;

Document::Document() : dirty_{false} {
    lines.emplace_back();  // add single empty line
}

Document::Document(std::filesystem::path file)
    : file_{std::move(file)}, dirty_{false} {
    namespace fs = std::filesystem;

    if (not fs::exists(file_)) {
        lines.emplace_back();
        return;
    }
    /*
    if (fs::exists(file_) and not fs::is_regular_file(file_)) {
        throw std::runtime_error{'\'' + file_.string() +
                                 "' is not regular file"};
    }
*/

    std::ifstream ifile{file_};
    if (!ifile) {
        throw std::runtime_error{"Cannot open file named '" + file_.string() +
                                 '\''};
    }

    // (serpant) TODO: skip copying from string to vector
    for (string ln; getline(ifile, ln);) {
        vector<Extended_char> line{ln.begin(), ln.end()};

        lines.push_back(std::move(line));
        // we no longer need line so we move
        // HINT: never mix const and move because it will invoke copying
    }

    if (lines.empty()) {
        lines.emplace_back();
    }

    tab_to_spaces();
}

void Document::tab_to_spaces() {
    for (auto& line : lines) {
        auto tab_pos = find(line.begin(), line.end(), '\t');
        while (tab_pos != line.end()) {
            auto insert_pos = line.erase(tab_pos);  // after erased position
            auto start = line.insert(insert_pos, settings::tab_size,
                                     ' ');  // inserted value pos

            tab_pos = find(start, line.end(), '\t');
        }
    }
}

std::string Document::getExtension() const { return file_.extension(); }

FileType Document::getFileType() const {
    const string& extension = this->getExtension();
    if (extension.empty()) {
        return FileType::None;
    }

    if (is_in(extension, ".c", ".h")) {
        return FileType::c;
    }
    if (is_in(extension, ".cpp", ".cc", ".hpp")) {
        // assume that c++ header are only hpp
        return FileType::cpp;
    }
    if (extension == ".py") {
        return FileType::python;
    }
    if (extension == ".go") {
        return FileType::go;
    }
    if (extension == ".rs") {
        return FileType::rust;
    }

    return FileType::None;
}

auto Document::save(const string& new_filename) -> uintmax_t
// new_filename default = "" when argument not given
// return the number of bytes written
{
    namespace fs = std::filesystem;
    if (file_.empty()) {
        if (new_filename.empty()) {
            throw Abort_save{"Save aborted"};
        }
        file_ = new_filename;
    }

    if (fs::exists(file_) and not fs::is_regular_file(file_)) {
        throw std::runtime_error{'\'' + file_.string() +
                                 "' already exists and is not regular file "
                                 "thus cannot be overwritten"};
    }

    std::ofstream file{file_};
    if (!file) {
        throw std::runtime_error{"Can't save! I/O error with file " +
                                 file_.string()};
    }

    for (const auto& line : lines) {
        for (const char ch : line) {
            file << ch;
        }
        file << '\n';
    }
    dirty_ = false;  // reset

    return fs::file_size(file_);
}
