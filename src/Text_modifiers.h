#ifndef TEXT_MODIFIERS_H
#define TEXT_MODIFIERS_H

#include <iostream>

enum class TextColor {
    reset = 0,
    reverse = 7,
    black = 30,
    red = 31,
    green = 32,
    yellow = 33,
    blue = 34,
    purple = 35,
    cyan = 36,
    white = 37
};

enum class BgColor {
    Reset = 0,
    Black = 40,
    Red = 41,
    Green = 42,
    Yellow = 43,
    Blue = 44,
    Magenta = 45,
    Cyan = 46,
    White = 47,
    Default = 49
};

enum class TextStyle {
    clear = 0,
    bold = 1,
    underscore = 4,
};

std::ostream& operator<<(std::ostream& os, TextColor c);
std::ostream& operator<<(std::ostream& os, BgColor c);
std::ostream& operator<<(std::ostream& os, TextStyle c);

#endif  // TEXT_MODIFIERS_H
