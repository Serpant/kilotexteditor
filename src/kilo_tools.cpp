#include "kilo_tools.h"

#include <iostream>

void alternate_screen(bool change) {
    constexpr std::string_view alt_screen{"\x1B[?1049h"};
    constexpr std::string_view restore_screen{"\x1B[?1049l"};

    if (change) {
        std::cout << alt_screen;

    } else {
        std::cout << restore_screen;
    }
}

void cursor_visibility(bool visible) {
    constexpr std::string_view cursor_invisible{"\x1B[?25l"};
    constexpr std::string_view cursor_visible{"\x1B[?25h"};

    if (visible) {
        std::cout << cursor_visible;
    } else {
        std::cout << cursor_invisible;
    }
}

void cursor_move(int cy, int cx) {
    // we add one because terminal counts from 1
    std::string cursor_pos =
        "\x1B[" + std::to_string(cy + 1) + ";" + std::to_string(cx + 1) + "H";

    std::cout << cursor_pos;
}

// clears chars from current position to the end
void clear_line() { std::cout << "\x1B[K"; }
