#include "read_key.h"

#include "Key.h"

using std::array;
using std::cin;

int readKey() {
    int ch = cin.get();

    if (ch == Key::esc) {
        array<char, 3> seq;

        seq[0] = cin.get();
        if (seq[0] == '[') {
            seq[1] = cin.get();
            if (seq[1] >= '0' && seq[1] <= '9') {
                seq[2] = cin.get();
                if (seq[2] == '~') {
                    switch (seq[1]) {
                        case '1':
                            return Key::home;
                        case '3':
                            return Key::del;
                        case '4':
                            return Key::end;
                        case '5':
                            return Key::page_up;
                        case '6':
                            return Key::page_down;
                        case '7':
                            return Key::home;
                        case '8':
                            return Key::end;
                    }
                }
            } else {
                switch (seq[1]) {
                    case 'A':
                        return Key::arrow_up;
                    case 'B':
                        return Key::arrow_down;
                    case 'C':
                        return Key::arrow_right;
                    case 'D':
                        return Key::arrow_left;
                    case 'H':
                        return Key::home;
                    case 'F':
                        return Key::end;
                }
            }
        } else if (seq[0] == 'O') {
            switch (seq[1]) {
                case 'H':
                    return Key::home;
                case 'F':
                    return Key::end;
            }
        }

        cin.unget();  // plain escape we dont use character
        return Key::esc;
        // return Key::esc;
    } else
        return ch;
}
