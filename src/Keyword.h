#ifndef KEYWORD_CLASS_H
#define KEYWORD_CLASS_H

#include <map>
#include <set>
#include <vector>

#include "Document.h"
#include "Extended_char.h"

class Keyword {
    using Line = std::vector<Extended_char>;

public:
    Keyword(Line::iterator first, Line::iterator end, HighlightType type)
        : pos{first}, size{Line::size_type(end - first)}, hl_type{type} {}
    Keyword(Line::iterator first, Line::size_type end, HighlightType type)
        : pos{first}, size{end}, hl_type{type} {}
    auto begin() { return pos; }
    auto end() { return pos + size; }

    void setHighLight(HighlightType type) { hl_type = type; }
    auto getHighLight() { return hl_type; };

private:
    Line::iterator pos;
    Line::size_type size;
    HighlightType hl_type;
};

auto getKeyWords(FileType type) -> std::map<std::string_view, HighlightType>;
#endif  // KEYWORD_CLASS_H
