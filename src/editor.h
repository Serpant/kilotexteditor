#ifndef EDITOR_H
#define EDITOR_H

#include <list>
#include <memory>
#include <vector>

#include "Mark.h"
#include "Screen.h"
#include "Terminal.h"

using Line = std::vector<Extended_char>;

class Terminal;  // forward delcaration because we use reference

namespace Editor {

enum class State { run = 0, quit_confirmed = 1, quit = 2 };

class TextEditor {
public:
    explicit TextEditor(const std::vector<std::string>& filenames);

    void quit();

    void run();
    [[nodiscard]] bool is_running() const { return state != State::quit; };

    void processKeyPress();

    void prev_file();
    void next_file();

    [[nodiscard]] Document& file() { return *file_; }
    [[nodiscard]] const Document& file() const { return *file_; }

    void save();
    void open_file();
    void close_file();

    void search();
    void insertNewLine(Mark& mark);
    void delChar(Mark& mark) const;
    void insertChar(Mark& mark, int ch);

private:
    std::vector<Document> files_;
    std::vector<Document>::iterator file_;

    State state = State::run;
};

void rowDelChar(std::list<Line>::iterator row, Line::size_type pos);
void join_line(std::list<Line>::iterator base, std::list<Line>::iterator line);
void delChar(Mark& mark);
void rowInsertChar(std::list<Line>::iterator row, Line::size_type at,
                   const int ch);
void append_line(Document& doc);

bool match(Text_iterator first, Text_iterator last, const std::string& query);

auto find_txt(Text_iterator first, Text_iterator last, const std::string& query)
    -> std::vector<Text_iterator>;

void confirmQuit(State& state);

};      // namespace Editor
#endif  // EDITOR_H
