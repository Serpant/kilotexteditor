#ifndef SCREEN_H
#define SCREEN_H

#include <list>
#include <memory>
#include <vector>

#include "Document.h"
#include "Mark.h"
#include "Status.h"

// Get terminal size libraries
#include <sys/ioctl.h>
#include <unistd.h>

using Line = std::vector<Extended_char>;

class Screen {
public:
    Screen();
    ~Screen();

    void refresh();

    [[nodiscard]] auto columns() const { return m_columns; }
    [[nodiscard]] auto rows() const { return m_rows; }

    [[nodiscard]] auto linesBegin() const { return firstPrintedLine; };
    [[nodiscard]] auto linesEnd() const;

    [[nodiscard]] std::list<Line>::size_type lineNumber() const {
        return distance(m_doc->lineBegin(), firstPrintedLine);
    }

    void setColumnOffest(const Line::size_type new_off) {
        column_offset = new_off;
    }
    [[nodiscard]] auto colOff() const { return column_offset; }

    void attach(Mark& mark, Document& doc) {
        m_doc = &doc;
        m_mark = &mark;

        firstPrintedLine = m_doc->lineBegin();
        column_offset = 0;
    }

    Status status;

private:
    void drawRows() const;
    void drawStatusBar();
    void drawStatusMessage() const;
    void scroll();
    void rowsUpdateSyntax();

    Line::size_type column_offset;
    std::list<Line>::iterator firstPrintedLine;

    const Line::size_type m_columns;
    const std::list<Line>::size_type m_rows;

    Document* m_doc;
    Mark* m_mark;
};

auto getWindowSize() -> std::pair<Line::size_type, std::list<Line>::size_type>;
void adjustScreenOffset();
void adjustScreenLine(Screen& screen, const Mark& mark);
void printWelcomeMsg(int columns, std::string_view welcome);
auto getLongestScreenLineSize(const Screen& screen);

void markMoveScreenTop();
void markMoveScreenBottom();

void print_line(Line::iterator first, Line::iterator Last);

#endif  // SCREEN_H
