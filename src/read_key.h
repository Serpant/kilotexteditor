#ifndef READ_KEY_FUNCTION_H
#define READ_KEY_FUNCTION_H

#include <array>
#include <iostream>

int readKey();

#endif  // READ_KEY_FUNCTION_H
