#include "Text_iterator.h"

Text_iterator& Text_iterator::operator++() {
    if (index_ < ln_->size())
        ++index_;
    else {
        index_ = 0;
        ++ln_;
        while (ln_ != end_ && ln_->empty()) {
            ++ln_;  // skip empty lines
        }
    }

    return *this;
}
